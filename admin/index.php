<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>VELICHAM</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="css/owl.carousel.css" rel="stylesheet" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a><a href="#" class="logo_right"></a> </div>
  <div class="clearer"></div>
</div>
<div id="contentarea" class="bglogin">
  <div class="container adminarea">
    <h5>Login Here</h5>
    <div class="loginbox">
	
<?php
session_start();
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }
 $_SESSION['msg']='';
?>
	
<h2> <a style="color:#ED1C24" href="http://bodhiinfo.com/demo/velicham-result">Use this link </a></h2>
					
      <h6>Please provide your details</h6>
       <form action="do.php" class="login clearfix" method="post">
        <div class="row">
          <div class="col-sm-6">
            <div class="input-group">
              <div class="input-group-addon"><span class="adminicon"></span></div>
                     
              <input type="text" name="userName" id="userName" class="form-control" placeholder="Username" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="input-group">
              <div class="input-group-addon"><span class="passwordicon"></span></div>
              <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="input-group">
              <div class="input-group-addon"><span class="roleicon"></span></div>
              <select name="type" id="type" class="form-control" required>
				<option value="">User Type</option>
				<option value="convener">General Convener</option>	
				<option value="mandalam">Mandalam</option>
				<option value="controller">Controller</option>
			  </select>
            </div>
          </div>
		  
           
           <div class="col-sm-6">  
       <!--      <span class="submit"><input class="loginbtn" type="submit" value="Login" name="save" id="save" ></span>  -->
           </div>
		  <!-- <div class="col-sm-6">  
             <div class="form-group">
               <label>
                   <input type="checkbox" name="remember" id="remember"><span class="checkboxtext"> Remember me</span>
                  </label>
                </div>
           </div>-->
         </div>
		 
		 
      </form>
      <!--<div class="text-center password"><a href="#">Forgot your password?</a></div>-->
    </div>
  </div>
  <div id="footer">
    <div class="container">
      <div class="powerd">
        <p>Powered By </p>
        <div class="powerdimg"><a href="http://www.bodhiinfo.com/" target="_blank"><img src="img/bodhi.png"  alt=""/></a></div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>