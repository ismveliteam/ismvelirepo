$(document).ready(function(){
//$(function () {
    // Define function for pagination
    function pagination(selector, dataPerPage, pagerSelector) {
        var tableSelector = $(selector);
        var countPerPage = parseInt(dataPerPage);
        var pagerTarget = $(pagerSelector);
        //console.log(tableSelector[0]);
        //console.log(countPerPage);

        var tableLength = tableSelector.find('tbody:first > tr').length;
        //console.log(tableLength);

        var totalNoPages = Math.ceil(tableLength / countPerPage);
        //console.log(totalNoPages);
        
        if (totalNoPages === 1){
            tableSelector.find('tbody:first > tr').addClass('active');
            pagerTarget.remove();
        }

        // Show first 'countPerpage' elements
        for (var i = 0; i <= countPerPage; i++) {
            tableSelector.find('tbody:first > tr:nth-child(' + i + ')').addClass('active');
        }

        //function for arranging the pager btns
        function pagNavSetter() {
            // DOM Ready settings
            var firstPrevBtns = '<button class="btn btn-success btn_pager_nav btn_first"><i class="fa fa-fast-backward"></i></button><button class="btn btn-success btn_pager_nav btn_prev"><i class="fa fa-caret-left"></i></button>';
            var lastNextBtns = '<button class="btn btn-success btn_pager_nav btn_next"><i class="fa fa-caret-right"></i></button><button class="btn btn-success btn_pager_nav btn_last"><i class="fa fa-fast-forward"></i></button>';
            if (totalNoPages >= 5) {
                pagerTarget.append(firstPrevBtns + '<button class="btn btn-success btn_pager_nav numbered_btn active">1</button>');
                for (var i = 2; i <= 5; i++) {
                    pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn">' + i + '</button>');
                }
                pagerTarget.append(lastNextBtns);
            } else {
                pagerTarget.append(firstPrevBtns + '<button class="btn btn-success btn_pager_nav numbered_btn active">1</button>');
                for (var i = 2; i <= totalNoPages; i++) {
                    pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn">' + i + '</button>');
                }
                pagerTarget.append(lastNextBtns);
            }

        }

        pagNavSetter();

        $(document).on('click', '.btn_pager_nav', function () {
            var curBtn = $(this);

            function pagerActive() {
                /*$('.btn_pager_nav').not(curBtn).removeClass('active');
                curBtn.addClass('active');*/
                var curActivePager = pagerTarget.find('.active');
                if (curBtn.hasClass('btn_next')) {
                    //curActivePager.next().addClass('active');
                    if (curActivePager.next().text() !== '') {
                        curActivePager.removeClass('active');
                        curActivePager.next().addClass('active');
                    }
                } else if (curBtn.hasClass('btn_prev')) {
                    if (curActivePager.prev().text() !== '') {
                        curActivePager.removeClass('active');
                        curActivePager.prev().addClass('active');
                    }
                } else if (curBtn.hasClass('btn_first')) {
                    curActivePager.removeClass('active');
                    pagerTarget.find('.numbered_btn:first').addClass('active');
                } else if (curBtn.hasClass('btn_last')) {
                    curActivePager.removeClass('active');
                    pagerTarget.find('.numbered_btn:last').addClass('active');
                } else if (curBtn.hasClass('numbered_btn')) {
                    pagerTarget.find('.numbered_btn').not(curBtn).removeClass('active');
                    curBtn.addClass('active');
                }
            }
            pagerActive();

            // function for updating pager btns
            function pagerUpdate() {
                var intCurLastBtnTxt = parseInt(pagerTarget.find('.numbered_btn:last').text());
                var intCurFirstBtnTxt = parseInt(pagerTarget.find('.numbered_btn:first').text());
                var posCurPagerBtn = pagerTarget.find('.active').index() - 1;
                if (curBtn.hasClass('btn_next')) {
                    if ((totalNoPages > intCurLastBtnTxt) && (posCurPagerBtn > 3)) {
                        //console.log(posCurPagerBtn);
                        pagerTarget.find('.numbered_btn:first').remove();
                        pagerTarget.find('.btn_next').before('<button class="btn btn-success btn_pager_nav numbered_btn">' + (intCurLastBtnTxt + 1) + '</button>');
                    }
                } else if (curBtn.hasClass('btn_prev')) {
                    if ((totalNoPages > 5) && (intCurFirstBtnTxt > 1) && (posCurPagerBtn < 3)) {
                        pagerTarget.find('.numbered_btn:last').remove();
                        pagerTarget.find('.btn_prev').after('<button class="btn btn-success btn_pager_nav numbered_btn">' + (intCurFirstBtnTxt - 1) + '</button>');
                    }
                } else if (curBtn.hasClass('numbered_btn')) {
                    var targetPagerBtn = parseInt(curBtn.text());
                    if ((totalNoPages > 5) && (totalNoPages > targetPagerBtn)) {
                        if ((posCurPagerBtn > 3) && (targetPagerBtn !== (totalNoPages - 1))) {
                            pagerTarget.find('.numbered_btn:first').remove();
                            pagerTarget.find('.btn_next').before('<button class="btn btn-success btn_pager_nav numbered_btn">' + (intCurLastBtnTxt + 1) + '</button>');
                        } else if ((posCurPagerBtn < 3) && (intCurFirstBtnTxt > 1)) {
                            pagerTarget.find('.numbered_btn:last').remove();
                            pagerTarget.find('.btn_prev').after('<button class="btn btn-success btn_pager_nav numbered_btn">' + (intCurFirstBtnTxt - 1) + '</button>');
                        }
                    }
                }
            }
            pagerUpdate();


            var CurActiveLastTr = tableSelector.find('tbody:first > tr.active:last');
            //console.log(CurActiveLastTr);
            var indCurActiveLastTr = CurActiveLastTr.index();
            //console.log(indCurActiveLastTr);
            var curActiveFirstTr = tableSelector.find('tbody:first > tr.active:first');
            var indCurActiveFirstTr = curActiveFirstTr.index();
            //console.log(indCurActiveFirstTr);
            var curPageNumber = Math.floor(indCurActiveFirstTr / countPerPage) + 1;



            //var indCurActiveLastTrWhole = tableSelector.find('tbody:first > tr:nth-child('+ indCurActiveLastTr +')')).index();
            var indCurActiveLastTrWhole = tableSelector.find('tbody:first > tr').index(CurActiveLastTr);
            //console.log(indCurActiveLastTrWhole);

            // Check whether the clicked btn is next
            if (curBtn.hasClass('btn_next')) {

                if (((tableLength - 1) - indCurActiveLastTrWhole) > countPerPage) {
                    tableSelector.find('tbody:first > tr.active').removeClass('active');
                    for (var j = indCurActiveLastTrWhole + 2; j <= indCurActiveLastTrWhole + (countPerPage + 1); j++) {
                        tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                    }
                } else {
                    if (((tableLength - 1) - indCurActiveLastTrWhole) !== 0) {
                        tableSelector.find('tbody:first > tr.active').removeClass('active');
                        for (var j = indCurActiveLastTrWhole + 2; j <= (tableSelector.find('tbody:first > tr:last').index() + 1); j++) {
                            tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                        }
                    }
                }
            } else if (curBtn.hasClass('btn_prev')) {
                if (indCurActiveFirstTr >= countPerPage) {
                    tableSelector.find('tbody:first > tr.active').removeClass('active');
                    for (var j = indCurActiveFirstTr - (countPerPage - 1); j <= indCurActiveFirstTr; j++) {
                        tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                    }
                }
            } else if (curBtn.hasClass('btn_first')) {
                if (pagerTarget.find('.numbered_btn:first').text() != 1) {
                    pagerTarget.find('.btn').remove();
                    var firstPrevBtns = '<button class="btn btn-success btn_pager_nav btn_first"><i class="fa fa-fast-backward"></i></button><button class="btn btn-success btn_pager_nav btn_prev"><i class="fa fa-caret-left"></i></button>';
                    var lastNextBtns = '<button class="btn btn-success btn_pager_nav btn_next"><i class="fa fa-caret-right"></i></button><button class="btn btn-success btn_pager_nav btn_last"><i class="fa fa-fast-forward"></i></button>';
                    if (totalNoPages > 5) {
                        pagerTarget.append(firstPrevBtns + '<button class="btn btn-success btn_pager_nav numbered_btn active">1</button>');
                        for (var i = 2; i <= 5; i++) {
                            pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn">' + i + '</button>');
                        }
                        pagerTarget.append(lastNextBtns);
                    } else {
                        pagerTarget.append(firstPrevBtns + '<button class="btn btn-success btn_pager_nav numbered_btn active">1</button>');
                        for (var i = 2; i <= totalNoPages; i++) {
                            pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn">' + i + '</button>');
                        }
                        pagerTarget.append(lastNextBtns);
                    }
                }
                tableSelector.find('tbody:first > tr.active').removeClass('active');
                for (var i = 0; i <= countPerPage; i++) {
                    tableSelector.find('tbody:first > tr:nth-child(' + i + ')').addClass('active');
                }
            } else if (curBtn.hasClass('btn_last')) {
                var firstPrevBtns = '<button class="btn btn-success btn_pager_nav btn_first"><i class="fa fa-fast-backward"></i></button><button class="btn btn-success btn_pager_nav btn_prev"><i class="fa fa-caret-left"></i></button>';
                var lastNextBtns = '<button class="btn btn-success btn_pager_nav btn_next"><i class="fa fa-caret-right"></i></button><button class="btn btn-success btn_pager_nav btn_last"><i class="fa fa-fast-forward"></i></button>';

                if (totalNoPages > 5) {
                    pagerTarget.find('.btn').remove();
                    pagerTarget.append(firstPrevBtns);
                    for (var b = (totalNoPages - 4); b <= ((totalNoPages - 5) + 4); b++) {
                        pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn">' + b + '</button>');
                    }
                    pagerTarget.append('<button class="btn btn-success btn_pager_nav numbered_btn active">' + totalNoPages + '</button>');
                    pagerTarget.append(lastNextBtns);
                }
                
                if (countPerPage === 1){
                    tableSelector.find('tbody:first > tr.active').removeClass('active');
                    tableSelector.find('tbody:first > tr:last').addClass('active');
                } else {
                    if ((tableLength % countPerPage) === 0) {
                        tableSelector.find('tbody:first > tr.active').removeClass('active');
                        for (var j = ((tableLength - countPerPage) + 1); j < tableLength + 1; j++) {
                            tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                        }
                    } else {
                        var inFirstTrOfLastPage = countPerPage * Math.floor(tableLength / countPerPage);
                        tableSelector.find('tbody:first > tr.active').removeClass('active');
                        for (var j = inFirstTrOfLastPage + 1; j <= tableLength; j++) {
                            tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                        }
                    }
                }
            } else {
                var targetPageNumber = parseInt($(this).text());
                //console.log(targetPageNumber);
                //console.log(curPageNumber);
                if (targetPageNumber > curPageNumber) {
                    tableSelector.find('tbody:first > tr.active').removeClass('active');
                    for (var j = (((targetPageNumber - 1) * countPerPage) + 1); j <= (((targetPageNumber - 1) * countPerPage) + countPerPage); j++) {
                        tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                    }
                } else if (targetPageNumber < curPageNumber) {
                    tableSelector.find('tbody:first > tr.active').removeClass('active');
                    for (var j = (((targetPageNumber * countPerPage) + 1) - countPerPage); j <= ((targetPageNumber * countPerPage)); j++) {
                        tableSelector.find('tbody:first > tr:nth-child(' + j + ')').addClass('active');
                    }
                }


            }
        });


        //console.log(tableLength);
        //console.log(totalNoPages);
        //console.log(tableLength % countPerPage);

    }
    pagination('.view_limitter',10, '.pager_selector');
    
});