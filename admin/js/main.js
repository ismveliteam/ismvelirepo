$(document).ready(function() {
	$('.leftmenu').prepend('<div class="mobmenu"></div>');
	$('.mobmenu').hide();
    mobileMenu();

    $(window).resize(mobileMenu);

    $('.datepicker').datepicker();
});

function mobileMenu(){
    if ($(window).width() <= 992){	
		 $('.mobmenu').show();
		 $('.leftmenu>ul').slideUp();
		 $('.mobmenu').click(function(){
		   $('.leftmenu>ul').stop().slideToggle();
		 });
	}
	else{
		$('.mobmenu').hide();
		$('.leftmenu>ul').slideDown();
	}	
}


// script for diary
$(document).ready(function() {
	$('.diary_navigation > li > a').click(function(e) {
		e.preventDefault();
		var curBtn = $(this);
		$('.diary_navigation > li > a').not(curBtn).removeClass('active');
		curBtn.addClass('active');
		var targetDiaryBlock = $(curBtn.attr('href'));
		$('.diary_block').not(targetDiaryBlock).removeClass('active');
		targetDiaryBlock.addClass('active');
	});
});	