<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="convener"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['districtName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
				$districtName	=	$App->convert($_REQUEST['districtName']);
				$districtName	=	ucwords(strtolower($districtName));				
								
				$userName 		=	trim($_POST['dUserName']);
				$userName1		=	mysql_real_escape_string(htmlentities($userName));
				
					$existId=$db->existValuesId(TABLE_DISTRICT,"districtName='$districtName' or dUserName='$userName1'");
					if($existId>0)
					{
					$_SESSION['msg']="District Name or UserName Is Already Exist";					
					header("location:index.php");					
					}
					else
					{
					
					$data['districtName']		=	$districtName;
					
					$password	=	trim($_POST['dPassword']);									
					$password1	=	mysql_real_escape_string(htmlentities($password));		
								
					$password2	 =	md5($password1); // Encrepted Password
					$password3	 =	mysql_real_escape_string(htmlentities($password2)); // Password to store the database
					
					
					$data['dUserName']			=	$App->convert($userName1);
					$data['dPassword']			=	$App->convert($password3);					
						
					$success=$db->query_insert(TABLE_DISTRICT,$data);	
					
					$data['districtKey']			=	'd123'.$success;	
					
					$success=$db->query_update(TABLE_DISTRICT,$data," ID='{$success}'");						
					$db->close();
					
						if($success)
						{
						$_SESSION['msg']="District Details Added Successfully";					
						header("location:index.php");	
						}	
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}				
					}
				}						
		break;		
	// EDIT SECTION
	case 'edit':
		$editId	=	$_REQUEST['id'];
			if(!$_REQUEST['districtName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";									
				header("location:edit.php?id=$editId");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$districtName	=	$App->convert($_REQUEST['districtName']);
				$districtName	=	ucwords(strtolower($districtName));				
				
					$existId=$db->existValuesId(TABLE_DISTRICT,"districtName='$districtName' and ID!=$editId");
					if($existId>0)
					{
					$_SESSION['msg']="District Name Is Already Exist";					
					header("location:index.php");					
					}
					else
					{
					
					$data['districtName']		=	$districtName;								
					
					$success=$db->query_update(TABLE_DISTRICT,$data," ID='{$editId}'");								
					$db->close();
					
					if($success)
						{
							$_SESSION['msg']="District Details updated Successfully";					
							header("location:index.php");	
						}
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}					
					}
				}						
		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_DISTRICT."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="District Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");		
		break;		
}
?>