<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="convener"))
{
header("location:../../logout.php");
}
$loginId=$_SESSION['LogID'];
$loginType=$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new': 
		
		if(!$_REQUEST['shakhaName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;				
				
				$panchayathId		=	$App->convert($_REQUEST['panchayathId']);	
				$shakhaName			=	$App->convert($_REQUEST['shakhaName']);
				$shakhaName			=	ucwords(strtolower($shakhaName));					
				
				$userName	=	trim($_POST['sUserName']);
				$userName1	=	mysql_real_escape_string(htmlentities($userName));
				
				$existId=$db->existValuesId(TABLE_SHAKHA,"(panchayathId='$panchayathId' and shakhaName='$shakhaName') or sUserName='$userName1'");
					if($existId>0)
					{
					$_SESSION['msg']="Unit Name or UserName Is Already Exist";					
					header("location:index.php");					
					}
					else
					{				
					$data['panchayathId']	=	$panchayathId;
					$data['shakhaName']		=	$shakhaName;
					
					$password	=	trim($_POST['sPassword']);									
					$password1	=	mysql_real_escape_string(htmlentities($password));	
					$cpassword	= 	trim($_POST['cPassword']);
					$cpassword1	=	mysql_real_escape_string(htmlentities($cpassword));	
								
					$password2	 =	md5($password1); // Encrepted Password
					$password3	 =	mysql_real_escape_string(htmlentities($password2)); // Password to store the database
					
					if($cpassword==$cpassword1)
					{
					$data['sUserName']			=	$App->convert($userName1);
					$data['sPassword']			=	$App->convert($password3);					
						
					$success=$db->query_insert(TABLE_SHAKHA,$data);	
					
					$data['shakhaKey']			=	's123'.$success;								
					$success = $db->query_update(TABLE_SHAKHA,$data," ID='{$success}'");
					
					$db->close();
					
					
					if($success)
						{
						$_SESSION['msg']="Unit Details Added Successfully";					
						header("location:index.php");
						}	
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}
					}
					else
					{
						$_SESSION['msg']="Incorrect Password";					
						header("location:index.php");
					}
					}
				}						
		break;		
	// EDIT SECTION
	case 'edit':
		$editId	=	$_REQUEST['id'];
			if(!$_REQUEST['shakhaName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";									
				header("location:edit.php?id=$editId");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
			
				$panchayathId		=	$App->convert($_REQUEST['panchayathId']);	
				$shakhaName			=	$App->convert($_REQUEST['shakhaName']);
				$shakhaName			=	ucwords(strtolower($shakhaName));					
				
					
					$existId=$db->existValuesId(TABLE_SHAKHA,"(panchayathId='$panchayathId' and shakhaName='$shakhaName' and ID!=$editId");
					if($existId>0)
					{
					$_SESSION['msg']="Unit Name Already Exist";					
					header("location:index.php");					
					}
					else
					{										
					$data['panchayathId']	=	$panchayathId;
					$data['shakhaName']		=	$shakhaName;						
					
					$success=$db->query_update(TABLE_SHAKHA,$data," ID='{$editId}'");								
					$db->close();
					
					
					if($success)
						{
						$_SESSION['msg']="Unit Details updated Successfully";					
						header("location:index.php");	
						}	
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}						
					}
				}						
		
		
		break;		
	
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];	
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
						
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_SHAKHA."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Panchayath Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");								
		break;	
}
?>