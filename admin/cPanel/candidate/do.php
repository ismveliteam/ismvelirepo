<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId = $_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':		
		if(!$_REQUEST['candidateName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
				$candidateName	=	$App->capitalize($_REQUEST['candidateName']);
				$mobile	        = 	$App->convert($_REQUEST['mobile']);
                                $regNo		= 	$App->convert($_REQUEST['regNo']);
					/* $existId=$db->existValuesId(TABLE_TBL_CANDIDATE," candidateName='$candidateName' AND mobile='$mobile'");
					if($existId>0)
					{
					$_SESSION['msg']=" Candidate Is Already Exist";					
					header("location:index.php");					
					}
                                        	
					$existId2=$db->existValuesId(TABLE_TBL_CANDIDATE," regNo='$regNo'");
					if($existId2>0)
					{
					$_SESSION['msg']="Register Number Is Already Exist";				
					header("location:index.php");					
					}
					else
					{ */	
					$data['unitId']				=	$App->convert($_REQUEST['shakhaId']);
					$data['regNo']				=	$regNo;
					$data['candidateName']		=	$candidateName;
					$data['mobile']				= 	$mobile;
					$data['gender']				=	$App->convert($_REQUEST['gender']);
					$data['address']			=	$App->convert($_REQUEST['address']);
					$data['pincode']			=	$App->convert($_REQUEST['pincode']);
					$data['email']				=	$App->convert($_REQUEST['email']);
					$data['religion']			=	$App->convert($_REQUEST['religion']);
					$data['loginId']			=	$loginId;
					$success=$db->query_insert(TABLE_TBL_CANDIDATE,$data);
					$db->close();
						if($success)
						{
							header("location:send_sms.php?m=$mobile&n=$candidateName");	
						}
						else
						{
						$_SESSION['msg']="Failed";	
						}									
					// }
				}
								
		break;		
		
		
	// EDIT SECTION
	case 'edit':
		$editId	=	$_REQUEST['id'];
			if(!$_REQUEST['candidateName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
				$candidateName	=	$App->capitalize($_REQUEST['candidateName']);
				$mobile		= 	$App->convert($_REQUEST['mobile']);
                                $regNo		= 	$App->convert($_REQUEST['regNo']);
				/* $existId=$db->existValuesId(TABLE_TBL_CANDIDATE," candidateName='$candidateName' AND mobile='$mobile' AND ID!='$editId'");
					if($existId>0)
					{
					$_SESSION['msg']=" Candidate Is Already Exist";					
					header("location:index.php");					
					}
						
					$existId2=$db->existValuesId(TABLE_TBL_CANDIDATE," regNo='$regNo' AND ID!='$editId'");
					if($existId2>0)
					{
					$_SESSION['msg']="Register Number Is Already Exist";				
					header("location:index.php");					
					}
					else
					{ */
					$data['unitId']				=	$App->convert($_REQUEST['shakhaId']);
					$data['regNo']				=	$regNo;
					$data['candidateName']		=	$candidateName;
					$data['mobile']				= 	$mobile;
					$data['gender']				=	$App->convert($_REQUEST['gender']);
					$data['address']			=	$App->convert($_REQUEST['address']);
					$data['pincode']			=	$App->convert($_REQUEST['pincode']);
					$data['email']				=	$App->convert($_REQUEST['email']);
					$data['religion']			=	$App->convert($_REQUEST['religion']);
					$data['loginId']			=	$loginId;
					
					$success=$db->query_update(TABLE_TBL_CANDIDATE,$data," ID='{$editId}'");
					$db->close();
					
						if($success)
						{
						$_SESSION['msg']="Candidate Details updated Successfully";		
						}	
						else
						{
						$_SESSION['msg']="Failed";	
						}
						header("location:index.php");				
					// }
				}						
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_TBL_CANDIDATE."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Candidate Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");		
		break;	
		case 'approval':
				$editId	=	$_REQUEST['id'];
				$db 	= new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;			
				
					$data['candiStatus']		=	'request';
										
					$success=$db->query_update(TABLE_TBL_CANDIDATE,$data," ID='{$editId}'");
					$db->close();
					
						if($success)
						{
						$_SESSION['msg']="Request for update data send Successfully";	
						}	
						else
						{
						$_SESSION['msg']="Failed";	
						}
						header("location:index.php");				
				
}
?>