<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new': 
		
		if(!$_REQUEST['controllerName'])
			{ 
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
					$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$userName			=	$App->convert($_REQUEST['username']);
				$password			=	$App->convert($_REQUEST['password']);
				$username1			=	mysql_real_escape_string(htmlentities($userName));
				$password1			=	mysql_real_escape_string(htmlentities($password));
						
				$existId	=	$db->existValuesId(TABLE_TBL_CONTROLLER,"userName='$username1'");
				
				if($existId)
				{
					$_SESSION['msg']="Username already exist";
				}
				
				else
				{
					$email					=	$App->convert($_REQUEST['email']);
					$data['controllerName']	=	$App->convert($_REQUEST['controllerName']);
					$data['mobile']			=	$App->convert($_REQUEST['mobile']);
					$data['email']	   		=	$App->convert($_REQUEST['email']);
					$data['userName']	   	=	$username1;
					$data['password']	   	=	md5($password1);
													
					$success		 =	$db->query_insert(TABLE_TBL_CONTROLLER,$data);
					if($success)
					{
						$subject	=	"ISM VELICHAM";
						$message	=	"UserName	=	".$username1."\r\n"."Password	=	".$password1;
						mail($email,$subject,$message);
					}			
					$db->close();
					
					
					if($success)
					{
					$_SESSION['msg']="Controller Added Successfully";										
					}	
					else
					{
					$_SESSION['msg']="Failed";										
					}
				}
				
				header("location:index.php");												
			}						
		break;		
		case 'edit':
					$editId = $_REQUEST['id'];
					if(!$_REQUEST['controllerName'])
					{
						$_SESSION['msg']="Error, Invalid Details!";					
						header("location:index.php");		
					}
				else
					{				
						$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
						$db->connect();
						$success=0;
						
						$data['controllerName']	=	$App->convert($_REQUEST['controllerName']);
						$data['mobile']			=	$App->convert($_REQUEST['mobile']);
						$data['email']	   		=	$App->convert($_REQUEST['email']);

						
						$success		=	$db->query_update(TABLE_TBL_CONTROLLER,$data," ID='{$editId}'");						
						$db->close();
												
						if($success)
							{
							$_SESSION['msg']="Controller Updated Successfully";											
							}	
							else
							{
							$_SESSION['msg']="Failed";												
							}
						header("location:index.php");													
						}	
		
		break;
	
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];
				$success=0;	
							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_TBL_CONTROLLER."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Controller Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");						
		break;	
		
}
?>