<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="convener"))
{
header("location:../../logout.php");
}
$loginId=$_SESSION['LogID'];
$loginType=$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new': 
		
		if(!$_REQUEST['panchayathName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$mandalamId		=	$App->convert($_REQUEST['mandalamId']);	
				$panchayathName	=	$App->convert($_REQUEST['panchayathName']);
				$panchayathName	=	ucwords(strtolower($panchayathName));						
				
				$userName 		=	trim($_POST['pUserName']);
				$userName1		=	mysql_real_escape_string(htmlentities($userName));
				
				$existId=$db->existValuesId(TABLE_PANCHAYATH,"(mandalamId='$mandalamId' and panchayathName='$panchayathName') or pUserName='$userName1'");
					if($existId>0)
					{
					$_SESSION['msg']="Panchayath Name or UserName Is Already Exist";					
					header("location:index.php");					
					}
					else
					{				
					$data['mandalamId']			=	$mandalamId;
					$data['panchayathName']		=	$panchayathName;
					
					$password	=	trim($_POST['pPassword']);		
					$cpassword	= 	trim($_POST['cPassword']);
					$cpassword1	= 	mysql_real_escape_string(htmlentities($cpassword));				
					$password1	=	mysql_real_escape_string(htmlentities($password));		
					if($password1==$cpassword1)
					{
					$password2	 =	md5($password1); // Encrepted Password
					$password3	 =	mysql_real_escape_string(htmlentities($password2)); // Password to store the database
					
					
					$data['pUserName']			=	$App->convert($userName1);
					$data['pPassword']			=	$App->convert($password3);					
						
					$success=$db->query_insert(TABLE_PANCHAYATH,$data);
					
					$data['panchayathKey']			=	'p123'.$success;
					
					$success = $db->query_update(TABLE_PANCHAYATH,$data," ID='{$success}'");
													
					$db->close();
					
					
					if($success)
						{
						$_SESSION['msg']="Panchayath Details Added Successfully";					
						header("location:index.php");
						}	
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}	
					}
					else
					{
						$_SESSION['msg']="Incorrect Password";					
						header("location:index.php");
					}
					}
				}						
		break;		
	// EDIT SECTION
	case 'edit':
		$editId	=	$_REQUEST['id'];
			if(!$_REQUEST['panchayathName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";									
				header("location:edit.php?id=$editId");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
			
				$mandalamId		=	$App->convert($_REQUEST['mandalamId']);	
				$panchayathName	=	$App->convert($_REQUEST['panchayathName']);
				$panchayathName	=	ucwords(strtolower($panchayathName));
					
					$existId=$db->existValuesId(TABLE_PANCHAYATH,"(mandalamId='$mandalamId' and panchayathName='$panchayathName' and ID!=$editId");
					if($existId>0)
					{
					$_SESSION['msg']="Panchayath Name Already Exist";					
					header("location:index.php");					
					}
					else
					{										
					$data['mandalamId']			=	$mandalamId;
					$data['panchayathName']		=	$panchayathName;						
					
					$success=$db->query_update(TABLE_PANCHAYATH,$data," ID='{$editId}'");								
					$db->close();
					
					
					if($success)
						{
						$_SESSION['msg']="Panchayath Details updated Successfully";					
						header("location:index.php");	
						}	
						else
						{
						$_SESSION['msg']="Failed";					
						header("location:index.php");	
						}						
					}
				}						
		
		
		break;		
	// DELETE SECTION		
		case 'delete':		
				$deleteId	=	$_REQUEST['id'];	
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
						
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_PANCHAYATH."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Panchayath Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");								
		break;		
}
?>