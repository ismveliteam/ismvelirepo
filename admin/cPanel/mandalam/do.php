<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if(($_SESSION['LogID']=="") ||($_SESSION['LogType']!="convener"))
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new': 
		
		if(!$_REQUEST['mandalamName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				
				$districtId		=	$App->convert($_REQUEST['districtId']);	
				$mandalamName	=	$App->convert($_REQUEST['mandalamName']);
				$mandalamName	=	ucwords(strtolower($mandalamName));			
				
				$userName 		=	trim($_POST['mUserName']);
				$userName1		=	mysql_real_escape_string(htmlentities($userName));
				
				
				$existId=$db->existValuesId(TABLE_MANDALAM,"(districtId='$districtId' and mandalamName='$mandalamName') or mUserName='$userName1'");
					if($existId>0)
					{
					$_SESSION['msg']="Mandalam Name or Username Is Already Exist";					
					header("location:index.php");					
					}
					else
					{
					$data['districtId']			=	$districtId;
					$data['mandalamName']		=	$mandalamName;				
					
					$password	=	trim($_POST['mPassword']);									
					$password1	=	mysql_real_escape_string(htmlentities($password));		
								
					$password2	 =	md5($password1); // Encrepted Password
					$password3	 =	mysql_real_escape_string(htmlentities($password2)); // Password to store the database
					
					
					$data['mUserName']			=	$App->convert($userName1);
					$data['mPassword']			=	$App->convert($password3);					
						
					$success=$db->query_insert(TABLE_MANDALAM,$data);	
					
					$data['mandalamKey']			=	'm123'.$success;
					
					$success = $db->query_update(TABLE_MANDALAM,$data," ID='{$success}'");
												
					$db->close();
															
					if($success)
						{
						$_SESSION['msg']="Mandalam Details Added Successfully";								
						}	
						else
						{
						$_SESSION['msg']="Failed";		
						}
										
						header("location:index.php");																
					}
				}						
		break;		
	// EDIT SECTION
	case 'edit':
		$editId	=	$_REQUEST['id'];
			if(!$_REQUEST['mandalamName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";									
				header("location:edit.php?id=$editId");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				
				$districtId		=	$App->convert($_REQUEST['districtId']);	
				$mandalamName	=	$App->convert($_REQUEST['mandalamName']);
				$mandalamName	=	ucwords(strtolower($mandalamName));		
				
					
					$existId=$db->existValuesId(TABLE_MANDALAM,"districtId='$districtId' and mandalamName='$mandalamName' and ID!=$editId");
					if($existId>0)
					{
					$_SESSION['msg']="Mandalam Name Already Exist";					
					header("location:index.php");					
					}
					else
					{					
					$data['districtId']			=	$districtId;
					$data['mandalamName']		=	$mandalamName;									
					
					$success=$db->query_update(TABLE_MANDALAM,$data," ID='{$editId}'");								
					$db->close();
										
					if($success)
						{
						$_SESSION['msg']="Mandalam Details updated Successfully";		
						}	
						else
						{
						$_SESSION['msg']="Failed";		
						}				
						header("location:index.php");				
					}
				}						
		
		
		break;		
	// DELETE SECTION
	case 'delete':		
				$deleteId	=	$_REQUEST['id'];	
				$success=0;				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
						
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_MANDALAM."` WHERE ID='{$deleteId}'");				      
				}
				catch(Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}											
				$db->close(); 
				if($success)
				{
					$_SESSION['msg']="Mandalam Details Deleted Successfully";										
				}	
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";										
				}	
				header("location:index.php");								
		break;		
}
?>