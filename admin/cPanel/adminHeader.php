<?php 
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$loginId=$_SESSION['LogID'];
$loginType=$_SESSION['LogType'];
$uid = $_SESSION['LogID'];
if($_SESSION['LogType']=='convener')
{
	$uname = 'convener'; 
}

else if($_SESSION['LogType']=='mandalam')
{
	$skaNameQry 	= 	mysql_query("SELECT mandalamName FROM ".TABLE_MANDALAM." where ID='$uid'");
	$skaName       =	mysql_fetch_array($skaNameQry);
	
	$uname = $skaName['mandalamName'];	
}
else if($_SESSION['LogType']=='controller')
{
	$exNameQry 	= 	mysql_query("SELECT controllerName FROM ".TABLE_TBL_CONTROLLER." where ID='$uid'");
	$exName       =	mysql_fetch_array($exNameQry);
	
	$uname = $exName['controllerName'];	
}
else
{
header("location:../../index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ISM</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery_pagination.js"></script>


<script src="chart/lib/js/jquery.min.js"></script>
<script src="chart/lib/js/chartphp.js"></script>
<link rel="stylesheet" href="chart/lib/js/chartphp.css">


<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/jQuery-pagination.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link href="../../css/newstyle.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="../../datePicker/jquery.datepick.css">
<script type="text/javascript" src="../../datePicker/jquery.datepick.js"></script>
<script type="text/javascript" src="../../datePicker/jquery.min.js"></script>
<script type="text/javascript" src="../../datePicker/jquery.plugin.js"></script>
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a>
    <div class="navigation">
      <a href="#" class="logo_right hd_in"></a>
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> <?php echo $uname;?> <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
          <ul>
          
		  <?php		  
			if($_SESSION['LogType']=="convener")
			{			
		  ?>
		   <li> <a href="../controller/"> <span class="menuicons icn2"></span>Controller</a></li>
<li> <a href="../district/"> <span class="menuicons icn2"></span>District</a></li>
		   <li> <a href="../mandalam/"> <span class="menuicons icn2"></span>Mandalam</a></li>
		   <li> <a href="../panchayath/"> <span class="menuicons icn2"></span>Panchayath</a></li>
		   <li> <a href="../shakha/"> <span class="menuicons icn2"></span>Unit</a></li>
		   <li> <a href="../publish/"> <span class="menuicons icn2"></span>Publish Result</a></li>
		  <li> <a href="../report/result.php"> <span class="menuicons icn2"></span>Result</a></li>
		  <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="menuicons icn1"></span>Report</a>
		  	<ul class="dropdown-menu dropdown2" role="menu">
	            
	             <li> <a href="../report/district.php"> <span class="menuicons icn2"></span>District</a></li> 
	             <li> <a href="../report/mandalam.php"> <span class="menuicons icn2"></span>Mandalam</a></li> 
	             <li> <a href="../report/panchayath.php"> <span class="menuicons icn2"></span>Panchayath</a></li> 
	             <li> <a href="../report/shakha.php"> <span class="menuicons icn2"></span>Unit</a></li> 
	             <li> <a href="../report/controller.php"> <span class="menuicons icn2"></span>controller</a></li> 
	             <li> <a href="../report/candidate.php"> <span class="menuicons icn2"></span>Candidate</a></li> 
             </ul>
           </li>
           <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="menuicons icn1"></span>Edit request</a>
		  	<ul class="dropdown-menu dropdown2" role="menu">
	            <li> <a href="../request/mark.php"> <span class="menuicons icn2"></span>Mark</a></li> 
	             <li> <a href="../request/candidate.php"> <span class="menuicons icn2"></span>Candidate</a></li> 
             </ul>
           </li>
            <?php 
			}				  						
			if($_SESSION['LogType']=="shakha")
			{			
		  ?>
            <li> <a href="../candidate/" class="dropdown-toggle" > <span class="menuicons icn1"></span> Candidate Reg </a></li>
            <li> <a href="../report/result.php"> <span class="menuicons icn2"></span>Result</a></li>
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="menuicons icn1"></span>Report</a>
		  	<ul class="dropdown-menu dropdown2" role="menu">	            
	             <li> <a href="../report/candidate.php"> <span class="menuicons icn2"></span>Candidate</a></li> 
             </ul>
           </li>
			<?php 
			}
			if($_SESSION['LogType']=="controller")
			{
			?>
				<!-- <li> <a href="../resultEntry/"><span class="menuicons icn4"></span>Result Entry</a></li>*/ -->
	            
			<?php
			}
			?>
           </ul>
        </div>
      </div>