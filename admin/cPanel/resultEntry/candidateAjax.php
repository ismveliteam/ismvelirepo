<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$regNo		=	$App->convert($_GET['regNo']);
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$candidate 	= $db->query_first("SELECT C.ID,C.regNo,C.mobile,C.candidateName,C.religion,C.mark,
									   S.shakhaName
								  FROM ".TABLE_TBL_CANDIDATE." C
							 LEFT JOIN ".TABLE_SHAKHA." S on S.ID=C.unitId
								 WHERE regNo='$regNo' ");
if($candidate)
{
?>
<div class="form-group">
	<label for="candidateName">Name:</label>
	<input type="text" name="candidateName" id="candidateName" class="form-control2" required=""  value="<?php echo $candidate['candidateName'];?>" disabled="" style="background-color: #e9e7e7">	
</div>
<div class="form-group">
	<label for="mobile">Mobile:</label>
	<input type="text" name="mobile" id="mobile" class="form-control2 "  value="<?php echo $candidate['mobile'];?>"  disabled="" style="background-color: #e9e7e7">
</div>
<div class="form-group">
	<label for="religion">Religion:</label>
	<input type="text" name="religion" id="religion" class="form-control2"  value="<?php echo $candidate['religion'];?>"  disabled="" style="background-color: #e9e7e7">
</div>
<div class="form-group">
	<label for="unitId">Unit:</label>
	<input type="text" name="unitId" id="unitId" class="form-control2"  value="<?php echo $candidate['shakhaName'];?>"  disabled="" style="background-color: #e9e7e7">
</div>
 <div class="form-group">
	<label for="mark">Mark:<span class="valid">*</span></label>
	<input type="text" name="mark" id="mark" class="form-control2" required="" value="<?php echo $candidate['mark'];?>"  >
 </div>
 
<?php 	
}
else
{
?>
<div class="form-group">
	<span class="valid"><div>Invalid Register number</div></span>
 </div>
<?php
}
?>