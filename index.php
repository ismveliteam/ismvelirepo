<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ISM - Kerala</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <div class="header">
        <div class="bg_pattern">
            <div class="container">
                <div class="logo">
                    <a target="_blank" href="http://ismkerala.org/"><img src="images/logo.png" /></a>
                </div>
                <div class="nav_wrap">
                    <ul class="navigation">
                        <li><a href="http://ismkerala.org/"><i class="fa fa-home"></i> Home</a></li>
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <div class="container">
        <div class="form_wrap">
            <form method="post" action="result/index.php">
                <div class="vel_logo">
                    <img src="images/home-logo.png" alt="" />
                </div>
                <h2 style="margin-bottom: 0px;">Exam Result 3</h2>
                <h3 class="text-center;" style=" color: #2d3393;">Surah Al-An'am</h3>
                <div class="form_block">
                    <label>Enter Registration No</label>                                        
                </div>
                <div class="form_block">
                    <input type="text" name="reg" />
                </div>
                <div class="form_block">
                    <input type="submit" name="submit" value="SUBMIT">
                </div>
                <div class="text-center" style="margin-top: 15px;">
                	Check District wise result <a href="district.php">here</a><br>
                	Check Mandalam wise result <a href="mandalam.php">here</a><br>
                	Check Panchayath wise result <a href="panchayath.php">here</a><br>
                	Check Unit wise result <a href="shakha.php">here</a>
                </div>
            </form>
        </div>
    </div>
    
    
    <div class="footer"></div>
    
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90747728-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
