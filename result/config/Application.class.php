<?php
session_start();
date_default_timezone_set('Asia/Kolkata'); 
class Application {
//Function : To encrypt and decrept string
//-
FUNCTION ENCRYPT_DECRYPT($Str_Message) {
//Function : encrypt/decrypt a string message v.1.0  without a known key
//Author   : Aitor Solozabal Merino (spain)
//Email    : aitor-3@euskalnet.net
//Date     : 01-04-2005
    $Len_Str_Message=STRLEN($Str_Message);
    $Str_Encrypted_Message="";
    FOR ($Position = 0;$Position<$Len_Str_Message;$Position++){
        // long code of the function to explain the algoritm
        //this function can be tailored by the programmer modifyng the formula
        //to calculate the key to use for every character in the string.
        $Key_To_Use = (($Len_Str_Message+$Position)+1); // (+5 or *3 or ^2)
        //after that we need a module division because can�t be greater than 255
        $Key_To_Use = (255+$Key_To_Use) % 255;
        $Byte_To_Be_Encrypted = SUBSTR($Str_Message, $Position, 1);
        $Ascii_Num_Byte_To_Encrypt = ORD($Byte_To_Be_Encrypted);
        $Xored_Byte = $Ascii_Num_Byte_To_Encrypt ^ $Key_To_Use;  //xor operation
        $Encrypted_Byte = CHR($Xored_Byte);
        $Str_Encrypted_Message .= $Encrypted_Byte;
       
        //short code of  the function once explained
        //$str_encrypted_message .= chr((ord(substr($str_message, $position, 1))) ^ ((255+(($len_str_message+$position)+1)) % 255));
    }
    RETURN $Str_Encrypted_Message;
} //end function


//Function : To stripslashes, mysql_real_escape_string, trim
//-
function convert($str)
 {
   $str1=stripslashes($str);
   $str2=trim($str1);
   return $str2;
 }
 
 
//Function : To split large sentence 
//-
function split_sentence($str,$count)
{
  if(strlen($str)>$count)
  {
   $str_content = substr($str, 0, $count);
   $str_pos = strrpos($str_content, " ");
   if ($str_pos>0)
    {
      $str_content = substr($str_content, 0, $str_pos);
      return $str_content."..."; 
	} 
   }
   else
   {
    return $str;
   }	
}
 
 
function showDate($dateTime, $format = "D, d-m-Y h:i A")
{
	$dateTime	=	strtotime($dateTime);
	return date($format,$dateTime);
}

function dbformat_date($exp_date) 
{
	$exp_date_original=$exp_date;
	
	  $exp_date1=explode("/",$exp_date);
	  if(count($exp_date1)==3)
	  {
	  	$exp_date_original=$exp_date1[2]."-".$exp_date1[1]."-".$exp_date1[0];
	  }
	  else
	  {
		$exp_date1=explode("-",$exp_date);
		$exp_date_original=$exp_date1[2]."/".$exp_date1[1]."/".$exp_date1[0];
	  }
	return $exp_date_original;
} 

function dbformat_time($exp_time) 
{
  $time =  date("H:i:s", strtotime($exp_time));
  return $time;
} 



//Function : To crope image thumb
//-

function cropImage($w,$h,$filename,$stype,$dest)
{

$filename = $filename;

$width = $w;
$height = $h;
switch($stype) {
        case 'gif':
        $simg = imagecreatefromgif($filename);
        break;
        case 'jpg':
        $simg = imagecreatefromjpeg($filename);
        break;
        case 'png':
        $simg = imagecreatefrompng($filename);
        break;
		
    }

header('Content-type: image/jpeg');

list($width_orig, $height_orig) = getimagesize($filename);

$ratio_orig = $width_orig/$height_orig;

if ($width/$height > $ratio_orig) 
  {
   $width = $height*$ratio_orig;
  } 
  else 
 {
   $height = $width/$ratio_orig;
}


$image_p = imagecreatetruecolor($width, $height);

imagecopyresampled($image_p, $simg, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

imagejpeg($image_p,$dest, 100);
}


};

$App = new Application();
//$App->path 	=	'http://localhost/dental';
//$App->path 	=	'http://moopans.dentricz.com



?>