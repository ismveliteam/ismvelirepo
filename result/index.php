<?php 
require("../admin/config/config.inc.php"); 
require("../admin/config/Database.class.php");
require("../admin/config/Application.class.php");

$regNo = $_REQUEST['reg'];
if(!$regNo)
{
	header("location:../index.php");
}


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$resultQry	=	"select ".TABLE_TBL_CANDIDATE.".ID,
					 ".TABLE_DISTRICT.".districtName as district,
					 ".TABLE_MANDALAM.".mandalamName as mandalam,
					 ".TABLE_PANCHAYATH.".panchayathName as panchayath,
					 ".TABLE_SHAKHA.".shakhaName as unit,
					 ".TABLE_TBL_CANDIDATE.".candidateName as name,
					 ".TABLE_TBL_CANDIDATE.".regNo,
					 ".TABLE_TBL_CANDIDATE.".mark as result
				from ".TABLE_TBL_CANDIDATE.",
					 ".TABLE_SHAKHA.",
					 ".TABLE_PANCHAYATH.",
					 ".TABLE_MANDALAM.",
					 ".TABLE_DISTRICT." 
				WHERE ".TABLE_TBL_CANDIDATE.".regNo = '$regNo' and ".TABLE_TBL_CANDIDATE.".unitId=".TABLE_SHAKHA.".ID 
					 and ".TABLE_SHAKHA.".panchayathId=".TABLE_PANCHAYATH.".ID 
					 and ".TABLE_PANCHAYATH.".mandalamId=".TABLE_MANDALAM.".ID 
					 and ".TABLE_MANDALAM.".districtId=".TABLE_DISTRICT.".ID 
				";
$result 	=	mysql_query($resultQry);
$resultNum	=	mysql_num_rows($result);
$resultRows	=	mysql_fetch_array($result);

$sel="select * from ".TABLE_TBL_RESULT_SETTINGS." WHERE publishStatus='1'";
$res	      =	  mysql_query($sel);
$resultNum2	=	mysql_num_rows($res); 

?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ISM - Kerala</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <div class="header">
        <div class="bg_pattern">
            <div class="container">
                <div class="logo">
                    <a target="_blank" href="http://ismkerala.org/"><img src="../images/logo.png" /></a>
                </div>
                <div class="nav_wrap">
                    <ul class="navigation">
                        <li><a href="http://velichamresult.ismkerala.org/"><i class="fa fa-home"></i> Home</a></li>
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <div class="container">
        <div class="result_wrap">
            <div class="vel_logo">
                <img src="../images/home-logo.png" alt="" />
            </div>
            <h3>exam result 3</h3>
            <div class="result_inner">
<?php  if(($resultNum2)==0){ ?>

<div class="result_in">
    <div class="status" style="color: #e30000">
         Result is not published!
    </div>
</div>
<?php
}
else
{

             if(($resultNum)>0){ ?>
                <div class="result_in">
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Reg No : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['regNo']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Name : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['name']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <!--<div class="result_row">
                        <div class="result_row_left">
                            <label>Phone : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['phone']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>-->
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>District : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['district']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Mandalam : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['mandalam']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Panchaayath : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['panchayath']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Unit : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['unit']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <div class="result_row">
                        <div class="result_row_left">
                            <label>Score : </label>                            
                        </div>
                        <div class="result_row_right">
                            <label><?php echo $resultRows['result']; ?></label>
                        </div>
                        <div class="bd_clear"></div>
                    </div>
                    <?php if($resultRows['result']==100){?>
                    <div class="status">
                        Congatulations!
                    </div>
                    <?php } ?>
                </div>
                <?php }else{ ?>
                <div class="result_in">
                <div class="status" style="color: #e30000">
                        Registration Number Not Exist!
                    </div>
                </div>
                <?php }
}

 ?>
            </div>
        </div>
    </div>
    <div class="footer"></div>
</body>
</html>
