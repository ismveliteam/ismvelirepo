<?php 
require("../admin/config/config.inc.php"); 
require("../admin/config/Database.class.php");
require("../admin/config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$district  	= 	$_REQUEST['district'];

$key	   	= 	$_REQUEST['key'];
$disQry		=	mysql_query("SELECT * FROM ".TABLE_DISTRICT." WHERE districtName LIKE '$district' AND districtKey = '$key' ");
$disRow		=	mysql_fetch_array($disQry);

if((!$district) or (mysql_num_rows($disQry)==0))
{
	header("location:../district.php");
}

/*$resultQry	=	"SELECT `ID`,`examId`,`district`,`mandalam`,`panchayath`,`unit`,`name`,`regNo`,`result` 
				 FROM ".TABLE_EXAM_RESULT." 
				 WHERE district = '$district'";
$result 	=	mysql_query($resultQry);
$resultNum	=	mysql_num_rows($result);*/

$selAllQuery="select ".TABLE_TBL_CANDIDATE.".ID,
					 ".TABLE_DISTRICT.".districtName,
					 ".TABLE_MANDALAM.".mandalamName,
					 ".TABLE_PANCHAYATH.".panchayathName,
					 ".TABLE_SHAKHA.".shakhaName,
					 ".TABLE_TBL_CANDIDATE.".candidateName,
					 ".TABLE_TBL_CANDIDATE.".regNo,
					 ".TABLE_TBL_CANDIDATE.".mark
				from ".TABLE_TBL_CANDIDATE.",
					 ".TABLE_SHAKHA.",
					 ".TABLE_PANCHAYATH.",
					 ".TABLE_MANDALAM.",
					 ".TABLE_DISTRICT." 
				WHERE ".TABLE_TBL_CANDIDATE.".unitId=".TABLE_SHAKHA.".ID 
					 and ".TABLE_SHAKHA.".panchayathId=".TABLE_PANCHAYATH.".ID 
					 and ".TABLE_PANCHAYATH.".mandalamId=".TABLE_MANDALAM.".ID 
					 and ".TABLE_MANDALAM.".districtId=".TABLE_DISTRICT.".ID 
					 and ".TABLE_DISTRICT.".districtName='$district'
					 and ".TABLE_DISTRICT.".districtKey='$key'  
					 order by ".TABLE_TBL_CANDIDATE.".candidateName ";
					//echo $selAllQuery;
					$result= $db->query($selAllQuery);
					$resultNum=mysql_num_rows($result);




?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ISM - Kerala</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
    <div class="header">
        <div class="bg_pattern">
            <div class="container">
                <div class="logo">
                    <a target="_blank" href="http://ismkerala.org/"><img src="../images/logo.png" /></a>
                </div>
                <div class="nav_wrap">
                    <ul class="navigation">
                        <li><a href="http://velichamresult.ismkerala.org/district.php"><i class="fa fa-home"></i> Home</a></li>
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <div class="container">
        <div class="result_wrap">
            <div class="vel_logo">
                <img src="../images/home-logo.png" alt="" />
            </div>
            <h3>exam result 3</h3>
            <div class="result_inner">
            <?php if(($resultNum)>0){ ?>
                <div class="table-responsive">
                    <table class="table table-bordered">
                    	<tr>
							<th>SlNo</th>
							<th>District</th>
							<th>Mandalam</th>
							<th>Panchayath</th>
							<th>Unit</th>
							<th>Name</th>
							<th>RegNo</th>
							<th>Mark</th>
						</tr>
                    	<?php
                    	$i = 0;
                    	while($resultRows	=	mysql_fetch_array($result)) 
                    	{
							?>
							<tr>
								<td><?php echo ++$i;?></td>
								<td><?php echo $resultRows['districtName']; ?></td>
								<td><?php echo $resultRows['mandalamName']; ?></td>
								<td><?php echo $resultRows['panchayathName']; ?></td>
								<td><?php echo $resultRows['shakhaName']; ?></td>
								<td><?php echo $resultRows['candidateName']; ?></td>
								<td><?php echo $resultRows['regNo']; ?></td>
								<td><?php echo $resultRows['mark']; ?></td>
							</tr>
							<?php
						}
                    	?>
                    </table>
                    
                    
                   
                   
                    <?php /*if($resultRows['result']==100){?>
                    <div class="status">
                        Congatulations!
                    </div>
                    <?php } */?>
                
                <?php }else{ ?>
               <!-- <div class="result_in">
                	<div class="status" style="color: #e30000">-->
                	<tr>
                		<td colspan="8" align="center"> There is no data!</td>
                	</tr>
                       
                    <!--</div>
                </div>-->
                <?php }?>
                </div>
            </div>
        </div>
    </div>
    <div class="footer"></div>
</body>
</html>
