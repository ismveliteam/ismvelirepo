<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ISM - Kerala</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
    
    
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/script.js"></script>
    
    
</head>
    
<body>
<?php 
require("admin/config/config.inc.php"); 
require("admin/config/Database.class.php");
require("admin/config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);
$db->connect();
?>
    <div class="header">
        <div class="bg_pattern">
            <div class="container">
                <div class="logo">
                    <a target="_blank" href="http://ismkerala.org/"><img src="images/logo.png" /></a>
                </div>
                <div class="nav_wrap">
                    <ul class="navigation">
                        <li><a href="http://ismkerala.org/"><i class="fa fa-home"></i> Home</a></li>
                    </ul>
                </div>
            </div>
        </div>        
    </div>
    <div class="container">
        <div class="form_wrap">
            <form method="post" action="result/district_result.php">
                <div class="vel_logo">
                    <img src="images/home-logo.png" alt="" />
                </div>
                <h2 style="margin-bottom: 0px;">Exam Result 3</h2>
                <h3 class="text-center;" style=" color: #2d3393;">Surah Al-An'am</h3>
                <div class="form_block">
                    <label>District</label>                                        
                </div>
                <div class="form_block">
                    <select name="district">
                    	<option value="">Select</option>
                    	<?php 
                    	$selDistrict 	= 	mysql_query("SELECT * FROM ".TABLE_DISTRICT." order by districtName");
                    	while($districtRow	=	mysql_fetch_array($selDistrict))
                    	{
                    	?>
                    	<option value="<?php echo $districtRow['districtName'];?>"><?php echo $districtRow['districtName'];?></option>
                    	<?php }?>
                    </select>
                </div>
                <div class="form_block">
                    <label>District Key</label>                                        
                </div>
                <div class="form_block">
                    <input type="text" name="key" />
                </div>
                <div class="form_block">
                    <input type="submit" name="submit" value="SUBMIT">
                </div>
                <div class="text-center" style="margin-top: 15px;">
                	Check individual result <a href="index.php">here</a>
                </div>
            </form>
        </div>
    </div>
    
    
    <div class="footer"></div>
</body>
</html>
